﻿using System;
using System.Linq;
using ConferenceManagement.DataAccess;
using ConferenceManagement.ViewModels.Login;
using ConferenceManagement.ViewModels.User;

namespace ConferenceManagement.Services.User
{
    public class UserManager
    {
        private readonly ConferenceManagementEntities _conferenceManagementContext;

        public UserManager()
        {
            _conferenceManagementContext = new ConferenceManagementEntities();
        }


        public void AddAuthorUser(CreateUserViewModel userViewModel)
        {
            var personToAdd = new DataAccess.Person
            {
                FullName = userViewModel.FullName,
                Affiliation = userViewModel.Affiliation,
                EmailAddress = userViewModel.EmailAddress
            };

            _conferenceManagementContext.Persons.Add(personToAdd);
            _conferenceManagementContext.SaveChanges();
            var personId = personToAdd.Id;

            var userToAdd = new DataAccess.User
            {
                Username = userViewModel.Username,
                Password = userViewModel.Password,
                PersonId = personId,
                RoleId = (int) ConferenceManagementRole.Author
            };

            _conferenceManagementContext.Users.Add(userToAdd);
            _conferenceManagementContext.SaveChanges();

            var authorToAdd = new DataAccess.Author
            {
                PersonId = personId
            };

            _conferenceManagementContext.Authors.Add(authorToAdd);
            _conferenceManagementContext.SaveChanges();
        }


        public void AddComiteeMemberUser(CreateUserViewModel userViewModel)
        {
            var personToAdd = new DataAccess.Person
            {
                FullName = userViewModel.FullName,
                Affiliation = userViewModel.Affiliation,
                EmailAddress = userViewModel.EmailAddress
            };

            _conferenceManagementContext.Persons.Add(personToAdd);
            _conferenceManagementContext.SaveChanges();
            var personId = personToAdd.Id;

            var userToAdd = new DataAccess.User
            {
                Username = userViewModel.Username,
                Password = userViewModel.Password,
                PersonId = personId,
                RoleId = (int) ConferenceManagementRole.ComiteeMember
            };

            _conferenceManagementContext.Users.Add(userToAdd);
            _conferenceManagementContext.SaveChanges();

            var memberToAdd = new DataAccess.ComiteeMember
            {
                PersonId = personId,
                WebPage = userViewModel.WebPage,
                ComiteeId = (int) userViewModel.SelectedComitee
            };

            _conferenceManagementContext.ComiteeMembers.Add(memberToAdd);
            _conferenceManagementContext.SaveChanges();
        }

        public int GetUserRole(int userId)
        {
            var user = _conferenceManagementContext.Users.FirstOrDefault(u => u.Id == userId);
            var role = _conferenceManagementContext.Roles.FirstOrDefault(r => r.Id == user.RoleId);

            return role.Id;
        }

        public int? Login(string modelUsername, string modelPassword)
        {
            var user = _conferenceManagementContext.Users.FirstOrDefault(
                u => u.Username == modelUsername && u.Password == modelPassword);

            return
                user?.Id; //daca user == null (adica nu exista sau e invalid username/password) returneaza null, altfel returneaza id-ul userului
        }

        public int GetCommiteeMemberIdByUserId(int userId)
        {
            var user = _conferenceManagementContext.Users.FirstOrDefault(u => u.Id == userId);
            if (user != null)
            {
                var comiteeMember = _conferenceManagementContext.ComiteeMembers.FirstOrDefault(c => c.PersonId == user.PersonId);
                if (comiteeMember != null)
                {
                    return comiteeMember.Id;
                }

                throw new Exception("No comitee member associated with the user");
            }
            throw new Exception("No such user");
        }
    }
}