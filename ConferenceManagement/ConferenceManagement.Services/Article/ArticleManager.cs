﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConferenceManagement.DataAccess;
using ConferenceManagement.ViewModels.Article;
using ConferenceManagement.ViewModels.Conference;

namespace ConferenceManagement.Services.Article
{
    public class ArticleManager
    {
        private readonly ConferenceManagementEntities _conferenceManagementEntities;

        public ArticleManager()
        {
            _conferenceManagementEntities = new ConferenceManagementEntities();
        }

        public int CreateArticle(ProposeArticleViewModel model, int userId)
        {
            var user = _conferenceManagementEntities.Users.FirstOrDefault(u => u.Id == userId);
            var person = _conferenceManagementEntities.Persons.FirstOrDefault(p => p.Id == user.PersonId);
            var author = _conferenceManagementEntities.Authors.FirstOrDefault(a => a.PersonId == person.Id);

            var articleToAdd = new DataAccess.Article()
            {
                Keywords = model.Keywords,
                Abstract = model.Abstract,
                Title = model.Title,
                Domain = model.Domain,
                FilePath = model.FilePath,
                FileName = model.FileName,
                Status = ArticleStatus.Proposed.ToString(),
                AuthorId = author.Id,
                ConferenceId = model.SelectedConferenceId
            };

            var article = _conferenceManagementEntities.Articles.Add(articleToAdd);
            _conferenceManagementEntities.SaveChanges();

            return article.Id;
        }

        public IEnumerable<ArticleViewModel> GetArticlesByUser(int userId)
        {
            var user = _conferenceManagementEntities.Users.FirstOrDefault(u => u.Id == userId);
            var person = _conferenceManagementEntities.Persons.FirstOrDefault(p => p.Id == user.PersonId);
            var author =
                _conferenceManagementEntities.Authors.FirstOrDefault(c => c.PersonId == person.Id);
            return _conferenceManagementEntities.Articles.Where(c => c.AuthorId == author.Id).Select(
                request => new ArticleViewModel
                {
                    Id = request.Id,
                    Keywords = request.Keywords,
                    Abstract = request.Abstract,
                    Title = request.Title,
                    Domain = request.Domain,
                    FileName = request.FileName,
                    FilePath = request.FilePath,
                    Status = request.Status,
                    AuthorId = request.AuthorId
                });
        }

        public IEnumerable<ArticleViewModel> GetAllArticles()
        {
            return _conferenceManagementEntities.Articles.Select(
                request => new ArticleViewModel
                {
                    Id = request.Id,
                    Keywords = request.Keywords,
                    Abstract = request.Abstract,
                    Title = request.Title,
                    Domain = request.Domain,
                    FileName = request.FileName,
                    FilePath = request.FilePath,
                    Status = request.Status,
                    AuthorId = request.AuthorId
                });
        }

        public ArticleViewModel GetArticleById(int id)
        {
            return _conferenceManagementEntities.Articles.Where(c => c.Id == id).Select(
                request => new ArticleViewModel()
                {
                    Id = request.Id,
                    Keywords = request.Keywords,
                    Abstract = request.Abstract,
                    Title = request.Title,
                    Domain = request.Domain,
                    FileName = request.FileName,
                    FilePath = request.FilePath,
                    Status = request.Status,
                    AuthorId = request.AuthorId,
                    ConferenceName = request.Conference.Name
                }).FirstOrDefault();
        }

        public void AddArticleReview(ArticleReviewViewModel articleReview)
        {
            _conferenceManagementEntities.ReviewedArticles.Add(new ReviewedArticle
            {
                ArticleId = articleReview.ArticleId,
                ReviewerId = articleReview.ReviewerId,
                Qualifier = articleReview.Qualifier,
                Observations = articleReview.Observations
            });
            _conferenceManagementEntities.SaveChanges();
        }
    }
}
