﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConferenceManagement.DataAccess;
using ConferenceManagement.ViewModels.Conference;

namespace ConferenceManagement.Services.Conference
{
    public class ConferenceManager
    {
        public readonly ConferenceManagementEntities ConferenceManagementEntities;

        public ConferenceManager()
        {
            ConferenceManagementEntities = new ConferenceManagementEntities();
        }

        public void AddConference(CreateConferenceViewModel model)
        {
            var conferenceToAdd = new DataAccess.Conference
            {
                Name = model.Name,
                PaperDeadline = model.PaperDeadline,
                EnrollDeadline = model.EnrollDeadline,
                BidDeadline = model.BidDeadline,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Place = model.Place,
                ComiteeId = (int) model.SelectedComiteeId
            };

            ConferenceManagementEntities.Conferences.Add(conferenceToAdd);
            ConferenceManagementEntities.SaveChanges();

            var conferenceId = conferenceToAdd.Id;
        }

        public IEnumerable<ConferenceViewModel> GetConferencesByUser(int userId)
        {
            var user = ConferenceManagementEntities.Users.FirstOrDefault(u => u.Id == userId);
            var person = ConferenceManagementEntities.Persons.FirstOrDefault(p => p.Id == user.PersonId);
            var comiteeMember =
                ConferenceManagementEntities.ComiteeMembers.FirstOrDefault(c => c.PersonId == person.Id);
            var comitee = ConferenceManagementEntities.Comitees.FirstOrDefault(c => c.Id == comiteeMember.ComiteeId);
            return ConferenceManagementEntities.Conferences.Where(c => c.ComiteeId == comitee.Id).Select(
                request => new ConferenceViewModel()
                {
                    Id = request.Id,
                    Name = request.Name,
                    PaperDeadline = request.PaperDeadline,
                    EnrollDeadline = request.EnrollDeadline,
                    BidDeadline = request.BidDeadline,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate,
                    Place = request.Place,
                    ComiteeId = (ConferenceManagementComitee)request.ComiteeId
                });
        }

        public IEnumerable<ConferenceViewModel> GetConferences()
        {
            return ConferenceManagementEntities.Conferences.Select(
                conference => new ConferenceViewModel()
                {
                    Id = conference.Id,
                    Name = conference.Name,
                    PaperDeadline = conference.PaperDeadline,
                    EnrollDeadline = conference.EnrollDeadline,
                    BidDeadline = conference.BidDeadline,
                    StartDate = conference.StartDate,
                    EndDate = conference.EndDate,
                    Place = conference.Place,
                    ComiteeId = (ConferenceManagementComitee)conference.ComiteeId
                });
        }

        public void UpdateRequest(ConferenceViewModel model)
        {
            var conference = ConferenceManagementEntities.Conferences.FirstOrDefault(r => r.Id == model.Id);

            if (conference != null)
            {
                conference.PaperDeadline = model.PaperDeadline;
                conference.BidDeadline = model.BidDeadline;
                conference.EnrollDeadline = model.EnrollDeadline;
            }
            try
            {
                ConferenceManagementEntities.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        public ConferenceViewModel GetConferenceById(int id)
        {
            return ConferenceManagementEntities.Conferences.Where(c => c.Id == id).Select(
                conference => new ConferenceViewModel()
                {
                    Id = conference.Id,
                    Name = conference.Name,
                    PaperDeadline = conference.PaperDeadline,
                    EnrollDeadline = conference.EnrollDeadline,
                    BidDeadline = conference.BidDeadline,
                    StartDate = conference.StartDate,
                    EndDate = conference.EndDate,
                    Place = conference.Place,
                    ComiteeId = (ConferenceManagementComitee)conference.ComiteeId
                }).FirstOrDefault();
        }

    }
}
