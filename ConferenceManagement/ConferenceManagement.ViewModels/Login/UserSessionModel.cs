﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagement.ViewModels.Login
{
     public class UserSessionModel
    {

        public int UserId { get; set; }
        public string Username { get; set; }
        public ConferenceManagementRole Role { get; set; }
    }

    public enum ConferenceManagementRole
    {
        Admin = 1,
        Author = 2,
        ComiteeMember = 3
    }
}