﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagement.ViewModels.Conference
{
    public class ConferenceViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime PaperDeadline { get; set; }

        public DateTime EnrollDeadline { get; set; }

        public DateTime BidDeadline { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Place { get; set; }

        public ConferenceManagementComitee ComiteeId { get; set; }
    }

    public enum ConferenceManagementComitee
    {
        Comitee1 = 1,
        Comitee2 = 2,
        Comitee3 = 3, 
        Comitee4 = 4
    }
}
