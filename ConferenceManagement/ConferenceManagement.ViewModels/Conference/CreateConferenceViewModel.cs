﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ConferenceManagement.ViewModels.Conference
{
    public class CreateConferenceViewModel
    {
        public string Name { get; set; }

        public DateTime PaperDeadline { get; set; }

        public DateTime EnrollDeadline { get; set; }

        public DateTime BidDeadline { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Place { get; set; }

        public IEnumerable<SelectListItem> Comitees { get; set; }

        public int SelectedComiteeId { get; set; }

    }
}
