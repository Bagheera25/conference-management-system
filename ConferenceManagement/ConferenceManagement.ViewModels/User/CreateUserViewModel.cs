﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ConferenceManagement.ViewModels.Conference;

namespace ConferenceManagement.ViewModels.User
{
    public class CreateUserViewModel
    {
        public string FullName { get; set; }

        public string EmailAddress { get; set; }

        public string Affiliation { get; set; }

        public string WebPage { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public int RoleId { get; set; }

        public IEnumerable<SelectListItem> Comitees { get; set; }

        public int SelectedComitee { get; set; }
    }
}
