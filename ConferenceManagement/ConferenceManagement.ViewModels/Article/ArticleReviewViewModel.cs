﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagement.ViewModels.Article
{
    public class ArticleReviewViewModel
    {
        public int ArticleId { get; set; }

        public int ReviewerId { get; set; }

        public int Qualifier { get; set; }

        public string Observations { get; set; }
    }
}
