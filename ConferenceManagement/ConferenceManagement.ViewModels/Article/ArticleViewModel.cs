﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagement.ViewModels.Article
{
    public class ArticleViewModel
    {
        public int Id { get; set; }

        public string Keywords { get; set; }

        public string Abstract { get; set; }

        public string Title { get; set; }

        public string Domain { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }
        
        public string Status { get; set; }
   
        public int AuthorId { get; set; }

        public string ConferenceName { get; set; }
 }

    public enum ArticleStatus
    {
        Proposed = 1,
        Accepted = 2,
        Rejected = 3
    }
}
