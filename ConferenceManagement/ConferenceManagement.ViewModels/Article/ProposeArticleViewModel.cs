﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ConferenceManagement.ViewModels.Article
{
    public class ProposeArticleViewModel
    {
        public string Keywords { get; set; }

        public string Abstract { get; set; }

        public string Title { get; set; }

        public string Domain { get; set; }

        public string FilePath { get; set; }

        public string FileName { get; set; }
        
        public int AuthorId { get; set; }

        public IEnumerable<SelectListItem> Conferences { get; set; }

        public int SelectedConferenceId { get; set; }

        [Required]
        public HttpPostedFileBase File { get; set; }
    }
}
