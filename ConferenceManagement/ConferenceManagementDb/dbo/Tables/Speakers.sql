﻿CREATE TABLE [dbo].[Speakers] (
    [Id]               INT      IDENTITY (1, 1) NOT NULL,
    [PresantationDate] DATETIME NOT NULL,
    [AuthorId]         INT      NOT NULL,
    CONSTRAINT [PK_Speakers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AuthorSpeaker] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[Authors] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_AuthorSpeaker]
    ON [dbo].[Speakers]([AuthorId] ASC);

