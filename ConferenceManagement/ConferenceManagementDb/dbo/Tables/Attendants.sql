﻿CREATE TABLE [dbo].[Attendants] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Section]  NVARCHAR (MAX) NOT NULL,
    [Payment]  NVARCHAR (MAX) NOT NULL,
    [PersonId] INT            NOT NULL,
    CONSTRAINT [PK_Attendants] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PersonAttendant] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Persons] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_PersonAttendant]
    ON [dbo].[Attendants]([PersonId] ASC);

