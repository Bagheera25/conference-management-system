﻿CREATE TABLE [dbo].[Conferences] (
    [Id]             INT            NOT NULL,
    [Name]           NVARCHAR (50)  NOT NULL,
    [PaperDeadline]  DATETIME       NOT NULL,
    [EnrollDeadline] DATETIME       NOT NULL,
    [BidDeadline]    DATETIME       NOT NULL,
    [StartDate]      DATETIME       NOT NULL,
    [EndDate]        DATETIME       NOT NULL,
    [Place]          NVARCHAR (MAX) NOT NULL,
    [ComiteeId]      INT            NOT NULL,
    CONSTRAINT [PK_Conferences] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Conferences_Comitees] FOREIGN KEY ([ComiteeId]) REFERENCES [dbo].[Comitees] ([Id])
);

