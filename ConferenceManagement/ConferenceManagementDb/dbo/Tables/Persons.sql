﻿CREATE TABLE [dbo].[Persons] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [FullName]     NVARCHAR (MAX) NOT NULL,
    [Affiliation]  NVARCHAR (50)  NULL,
    [EmailAddress] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Persons] PRIMARY KEY CLUSTERED ([Id] ASC)
);





