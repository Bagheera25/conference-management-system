﻿CREATE TABLE [dbo].[Authors] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [PersonId] INT NOT NULL,
    CONSTRAINT [PK_Authors] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PersonAuthor] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Persons] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_PersonAuthor]
    ON [dbo].[Authors]([PersonId] ASC);

