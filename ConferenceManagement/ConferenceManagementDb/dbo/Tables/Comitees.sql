﻿CREATE TABLE [dbo].[Comitees] (
    [Id]   INT           NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Comitees] PRIMARY KEY CLUSTERED ([Id] ASC)
);

