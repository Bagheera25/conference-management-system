﻿CREATE TABLE [dbo].[ComiteeMembers] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [PersonId]  INT           NOT NULL,
    [WebPage]   NVARCHAR (50) NULL,
    [ComiteeId] INT           NOT NULL,
    CONSTRAINT [PK_Reviewers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ComiteeMembers_Comitees] FOREIGN KEY ([ComiteeId]) REFERENCES [dbo].[Comitees] ([Id]),
    CONSTRAINT [FK_Reviewers_Persons] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Persons] ([Id])
);





