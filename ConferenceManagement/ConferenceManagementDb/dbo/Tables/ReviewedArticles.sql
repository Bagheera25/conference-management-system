﻿CREATE TABLE [dbo].[ReviewedArticles] (
    [ArticleId]    INT            NOT NULL,
    [ReviewerId]   INT            NOT NULL,
    [Observations] NVARCHAR (MAX) NULL,
    [Qualifier]    INT            NOT NULL,
    CONSTRAINT [PK_ReviewedArticles_1] PRIMARY KEY CLUSTERED ([ArticleId] ASC, [ReviewerId] ASC),
    CONSTRAINT [FK_ArticleReviewedArticle] FOREIGN KEY ([ArticleId]) REFERENCES [dbo].[Articles] ([Id]),
    CONSTRAINT [FK_ReviewerReviewedArticle] FOREIGN KEY ([ReviewerId]) REFERENCES [dbo].[ComiteeMembers] ([Id])
);






GO
CREATE NONCLUSTERED INDEX [IX_FK_ReviewerReviewedArticle]
    ON [dbo].[ReviewedArticles]([ReviewerId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_FK_ArticleReviewedArticle]
    ON [dbo].[ReviewedArticles]([ArticleId] ASC);

