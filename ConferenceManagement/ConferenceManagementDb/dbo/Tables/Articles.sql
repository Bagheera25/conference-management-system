﻿CREATE TABLE [dbo].[Articles] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Keywords]     NVARCHAR (MAX) NOT NULL,
    [Abstract]     NVARCHAR (MAX) NOT NULL,
    [Title]        NVARCHAR (MAX) NOT NULL,
    [Domain]       NVARCHAR (MAX) NOT NULL,
    [FilePath]     NVARCHAR (MAX) NULL,
    [FileName]     NVARCHAR (MAX) NULL,
    [Status]       NVARCHAR (MAX) NOT NULL,
    [AuthorId]     INT            NOT NULL,
    [ConferenceId] INT            NOT NULL,
    CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Articles_Conferences] FOREIGN KEY ([ConferenceId]) REFERENCES [dbo].[Conferences] ([Id]),
    CONSTRAINT [FK_AuthorArticle] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[Authors] ([Id])
);




GO
CREATE NONCLUSTERED INDEX [IX_FK_AuthorArticle]
    ON [dbo].[Articles]([AuthorId] ASC);

