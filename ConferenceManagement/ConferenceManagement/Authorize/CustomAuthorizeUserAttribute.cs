﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Services.User;
using ConferenceManagement.ViewModels.Login;

namespace ConferenceManagement.Authorize
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomAuthorizeUserAttribute : AuthorizeAttribute
    {
        private readonly UserManager _userManager = new UserManager();
        private readonly ConferenceManagementRole[] _roles;

        public CustomAuthorizeUserAttribute(params ConferenceManagementRole[] roles)
        {
            _roles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var user = httpContext.Session["user"] as UserSessionModel;
            if (user != null)
            {
                var roleId = _userManager.GetUserRole(user.UserId);
                if (!_roles.Contains((ConferenceManagementRole)roleId)) return false;
                return true;
            }

            return false;
        }
    }
}