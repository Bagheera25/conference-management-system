In order to use the application, you need to have installed:

SQL express 2016 https://www.microsoft.com/en-cy/sql-server/sql-server-editions-express
express is mandatory because of conn strings
SQL management studio
Node.js https://nodejs.org/en/

Steps to configure the application after installing everything from above:

1. go to open a command prompt at ContentWorkflowDocumentManager/DFM.WebApp (navigate to ContentWorkflowDocumentManager/DFM.WebApp in file explorer then write in path "cmd", then press enter)

2. Run the command npm -install (this will install the packages for frontend)

3. Create a new database called "DFM", leave it empty (the db must be created in sql express because the connection string looks for ./SQLEXPRESS)

4. open DFM.sln with visual studio 2015 and above (was not testet with versions below 2015)

5. Right click on Database project on the right hand side => schema compare

6. Select the database DFM from your computer.

7. Click "Compare", a bunch of changes should appear

8. Click "Update".

9. Now the project is up to date and the database has the required structure.

10. Go to db and create some users (you can insert any id`s you want for them)

11. Go back to VS, hit f5 (this will start the server)

12. Open a command prompt at ContentWorkflowDocumentManager/DFM.WebApp

13. Run the command npm -start (this will start the client)

14. In your browser (only tested on Chrome), go to localhost:3000 and login into application.

For any other questions: vasi30@gmail.com