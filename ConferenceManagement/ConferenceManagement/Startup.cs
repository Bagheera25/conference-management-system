﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ConferenceManagement.Startup))]
namespace ConferenceManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
