﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Authorize;
using ConferenceManagement.Services.Conference;
using ConferenceManagement.ViewModels.Conference;
using ConferenceManagement.ViewModels.Login;

namespace ConferenceManagement.Controllers
{
    [CustomAuthorizeUser(ConferenceManagementRole.Admin)]
    public class ConferenceController : Controller
    {
        private readonly ConferenceManager _conferenceManager;

        public ConferenceController()
        {
            _conferenceManager = new ConferenceManager();
        }

        // GET: Conference
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateConference()
        {
            var model = new CreateConferenceViewModel { Comitees= GetSelectListItems() };

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateConference(CreateConferenceViewModel model)
        {
            model.Comitees = GetSelectListItems();
            _conferenceManager.AddConference(model);
            return View("ConferenceAddedSuccessfully");
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember)]
        public ActionResult ListAllConferences()
        {
            var model = new List<ConferenceViewModel>();
            var currentUserId = ((UserSessionModel)Session["user"]).UserId;
            model = User.IsInRole(ConferenceManagementRole.ComiteeMember.ToString()) ? _conferenceManager.GetConferencesByUser(currentUserId).ToList() : _conferenceManager.GetConferences().ToList();
            return View(model);
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember)]
        public ActionResult EditConferences(int id)
        {
            var model = _conferenceManager.GetConferenceById(id);
            return View(model);
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember)]
        [HttpPost]
        public ActionResult EditConferences(ConferenceViewModel model)
        {
            _conferenceManager.UpdateRequest(model);

            return RedirectToAction("ListAllConferences");
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember)]
        public ActionResult ConferenceDetails(int id)
        {
            var model = _conferenceManager.GetConferenceById(id);
            return View(model);
        }

        private IEnumerable<SelectListItem> GetSelectListItems()
        {
            var selectList = new List<SelectListItem>
            {
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee1).ToString(),
                    Text = ConferenceManagementComitee.Comitee1.ToString()
                },
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee2).ToString(),
                    Text = ConferenceManagementComitee.Comitee2.ToString()
                },
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee3).ToString(),
                    Text = ConferenceManagementComitee.Comitee3.ToString()
                },
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee4).ToString(),
                    Text = ConferenceManagementComitee.Comitee4.ToString()
                }
            };
            return selectList;
        }
    }
}