﻿using System.Collections.Generic;
using System.Web.Mvc;
using ConferenceManagement.Authorize;
using ConferenceManagement.Services.User;
using ConferenceManagement.ViewModels.Conference;
using ConferenceManagement.ViewModels.Login;
using ConferenceManagement.ViewModels.User;

namespace ConferenceManagement.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager _userManager;

        public UserController()
        {
            _userManager = new UserManager();
        }
        
        public ActionResult CreateAuthor()
        {
            var model = new CreateUserViewModel { };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAuthor(CreateUserViewModel model)
        {
            _userManager.AddAuthorUser(model);
            return View();
        }

        [CustomAuthorizeUser(ConferenceManagementRole.Admin)]
        public ActionResult CreateComiteeMember()
        {
            var model = new CreateUserViewModel { Comitees = GetSelectListItems()};
            return View(model);
        }

        [HttpPost]
        [CustomAuthorizeUser(ConferenceManagementRole.Admin)]
        [ValidateAntiForgeryToken]
        public ActionResult CreateComiteeMember(CreateUserViewModel model)
        {
            _userManager.AddComiteeMemberUser(model);
            return View("AccountCreatedSuccessfully");
        }

        private IEnumerable<SelectListItem> GetSelectListItems()
        {
            var selectList = new List<SelectListItem>
            {
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee1).ToString(),
                    Text = ConferenceManagementComitee.Comitee1.ToString()
                },
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee2).ToString(),
                    Text = ConferenceManagementComitee.Comitee2.ToString()
                },
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee3).ToString(),
                    Text = ConferenceManagementComitee.Comitee3.ToString()
                },
                new SelectListItem{
                    Value = ((int)ConferenceManagementComitee.Comitee4).ToString(),
                    Text = ConferenceManagementComitee.Comitee4.ToString()
                }
            };
            return selectList;
        }

    }
}