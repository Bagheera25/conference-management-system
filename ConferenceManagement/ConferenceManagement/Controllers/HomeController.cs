﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Authorize;
using ConferenceManagement.ViewModels.Login;

namespace ConferenceManagement.Controllers
{
    [CustomAuthorizeUser(ConferenceManagementRole.Admin, ConferenceManagementRole.Author, ConferenceManagementRole.ComiteeMember)]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated || Session["user"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}