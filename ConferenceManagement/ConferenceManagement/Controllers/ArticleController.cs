﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceManagement.Authorize;
using ConferenceManagement.Services.Article;
using ConferenceManagement.Services.Conference;
using ConferenceManagement.Services.User;
using ConferenceManagement.ViewModels.Article;
using ConferenceManagement.ViewModels.Login;

namespace ConferenceManagement.Controllers
{
    [CustomAuthorizeUser(ConferenceManagementRole.Author)]
    public class ArticleController : Controller
    {
        private readonly ArticleManager _articleManager;
        private readonly UserManager _userManager;
        private readonly ConferenceManager _conferenceManager;

        public ArticleController()
        {
            _articleManager = new ArticleManager();
            _userManager = new UserManager();
            _conferenceManager = new ConferenceManager();
        }

        // GET: Article
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProposeArticle()
        {
            var model = new ProposeArticleViewModel();

            var conferences = _conferenceManager.GetConferences().Select(c => new SelectListItem
            {
                Text = c.Name,
                Value = c.Id.ToString()
            });

            model.Conferences = conferences;
            return View(model);
        }

        [HttpPost]
        public ActionResult ProposeArticle(ProposeArticleViewModel model)
        {
            var userId = ((UserSessionModel) Session["user"]).UserId;

            if (model.File.ContentLength > 0)
            {
                var fileName = model.File.FileName;
                var path = AppDomain.CurrentDomain.BaseDirectory + "Documents\\" + fileName;
                model.File.SaveAs(path);
                model.FileName = fileName;
                model.FilePath = path;
            }
            if (!ModelState.IsValid) return RedirectToAction("ListMyArticles");
            var articleId = _articleManager.CreateArticle(model, userId);
            return View("ArticleDetails", _articleManager.GetArticleById(articleId));
        }

        public ActionResult ListMyArticles()
        {
            var currentUserId = ((UserSessionModel) Session["user"]).UserId;
            var model = _articleManager.GetArticlesByUser(currentUserId).ToList();
            return View(model);
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember)]
        public ActionResult ListAllArticles()
        {
            var model = _articleManager.GetAllArticles().ToList();

            return View(model);
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember, ConferenceManagementRole.Author)]
        public ActionResult ArticleDetails(int id)
        {
            var model = _articleManager.GetArticleById(id);
            return View(model);
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember)]
        public ActionResult ReviewArticle(int articleId)
        {
            var article = _articleManager.GetArticleById(articleId);
            var model = new ArticleReviewViewModel
            {
                ArticleId = articleId,
            };

            return View(model);
        }

        [CustomAuthorizeUser(ConferenceManagementRole.ComiteeMember)]
        [HttpPost]
        public ActionResult ReviewArticle(ArticleReviewViewModel articleReview)
        {
            articleReview.ReviewerId = _userManager.GetCommiteeMemberIdByUserId(((UserSessionModel)Session["user"]).UserId);

            _articleManager.AddArticleReview(articleReview);

            return View("ListAllArticles", _articleManager.GetAllArticles());
        }
    }
}