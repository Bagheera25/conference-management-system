//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConferenceManagement.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReviewedArticle
    {
        public int Qualifier { get; set; }
        public int ArticleId { get; set; }
        public int ReviewerId { get; set; }
        public string Observations { get; set; }
    
        public virtual Article Article { get; set; }
        public virtual ComiteeMember ComiteeMember { get; set; }
    }
}
