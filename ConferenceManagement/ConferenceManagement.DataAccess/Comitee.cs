//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConferenceManagement.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Comitee
    {
        public Comitee()
        {
            this.ComiteeMembers = new HashSet<ComiteeMember>();
            this.Conferences = new HashSet<Conference>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<ComiteeMember> ComiteeMembers { get; set; }
        public virtual ICollection<Conference> Conferences { get; set; }
    }
}
